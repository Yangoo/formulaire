<?php
require "credentials.php"
try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd', $user, $password);
    $stmt=$dbh->query("SELECT * FROM utilisateurs");
    $resultats = $stmt->fetchAll();
}
catch(Exception $e){
    var_dump($e);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <h1> Table Animateur </h1>
    <table>
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Nom_Rue</th>
                <th>Num_Rue</th>
                <th>Code Postal</th>
                <th>Ville</th>
                <th>Mail</th>
                <th>Supprimer l'utilisateur</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultats as $key => $resultat): ?>
            <tr>
                <td><?php echo $resultat['Nom']; ?></td>
                <td><?php echo $resultat['Prenom']; ?></td>
                <td><?php echo $resultat['Nom_rue']; ?></td>
                <td><?php echo $resultat['Num_rue']; ?></td>
                <td><?php echo $resultat['Cp']; ?></td>
                <td><?php echo $resultat['Ville_Adr']; ?></td>
                <td><?php echo $resultat['Mail']; ?></td>
                <td><form method="POST" action="delete.php">
                    <input type="hidden" name="ID" value=<?php echo $resultat['ID']?>>
                    <input type="submit"></td>
            </tr>
            <?php endforeach; ?>
            <div class='row'>
                <div class ="col-12">
                <a href="login.html" class="btn btn-success">Se connecter</a></div>
        </tbody>
    </table>  
    <a href="Ajout.php" class="btn btn-success">Ajouter un utilisateur</a></div>
<?php require 'footer.php'; ?>     

</body>
</html>